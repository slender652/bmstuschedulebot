import logging.config
import yaml


class Logger:
    def __init__(self, name):
        self.name = name
        self.load_config('/app/config/logging-config.yml')

    def get_logger(self):
        return logging.getLogger(self.name)

    @staticmethod
    def load_config(config_path):
        with open(config_path, 'r') as f:
            config = yaml.safe_load(f.read())
            logging.config.dictConfig(config)
