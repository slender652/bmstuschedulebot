import sqlite3 as sqlite

from logger import Logger


class Database:
    create_table_sql = '''
        CREATE TABLE users (user_id, group_name);
    '''

    read_group_sql = '''
        SELECT group_name FROM users
        WHERE user_id = (?);
    '''

    write_group_sql = '''
        INSERT INTO users VALUES (?,?);
    '''

    update_group_sql = '''
        UPDATE users
        SET group_name = (?)
        WHERE user_id = (?)
    '''

    delete_group_sql = '''
        DELETE FROM users
        WHERE user_id = (?);
    '''

    logger = Logger('dbLogger').get_logger()

    def __init__(self, db_name):
        self.db_name = db_name
        self.connection = self.connect(db_name)
        self.cursor = self.connection.cursor()

        self.create_table()

    @staticmethod
    def connect(db_name):
        try:
            return sqlite.connect(db_name, check_same_thread=False)
        except sqlite.Error as e:
            Database.logger.error('DB error encountered!', exc_info=True)

    def create_table(self):
        try:
            self.cursor.execute(Database.create_table_sql)
            self.connection.commit()
        except sqlite.Error as e:
            self.connection.rollback()
            Database.logger.error('DB error encountered!', exc_info=True)

    def read_group(self, user_id):
        try:
            return self.cursor.execute(Database.read_group_sql, (user_id,)).fetchone()
        except sqlite.Error as e:
            Database.logger.error('DB error encountered!', exc_info=True)

    def write_group(self, user_id, group_name):
        try:
            self.cursor.execute(Database.write_group_sql, (user_id, group_name))
            self.connection.commit()
        except sqlite.Error as e:
            self.connection.rollback()
            Database.logger.error('DB error encountered!', exc_info=True)

    def update_group(self, user_id, group_name):
        try:
            self.cursor.execute(Database.update_group_sql, (group_name, user_id))
            self.connection.commit()
        except sqlite.Error as e:
            self.connection.rollback()
            Database.logger.error('DB error encountered!', exc_info=True)

    def delete_group(self, user_id):
        try:
            self.cursor.execute(Database.delete_group_sql, (user_id,))
            self.connection.commit()
        except sqlite.Error as e:
            self.connection.rollback()
            Database.logger.error('DB error encountered!', exc_info=True)
