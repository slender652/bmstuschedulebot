import requests
from exceptions import *
from lesson import *
from logger import Logger

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup


class ScheduleParser:
    logger = Logger('parserLogger').get_logger()

    def __init__(self, url, group):
        self.base_url = url  # Always equal to 'https://students.bmstu.ru/'
        self.group = group

    @staticmethod
    def load_page(url):
        session = requests.Session()
        request = session.get(url)
        return request.text

    def group_links_tags(self, tag):
        return (tag['name'] == 'a') & (tag['class'] == ['btn', 'btn-sm', 'btn-default', 'text-nowrap']) & \
               (tag.getText() == self.group)

    # Finds a link to a schedule for a group we are interested in
    def get_group_link(self):
        group_link = None
        soup = BeautifulSoup(self.load_page(self.base_url + 'schedule/list'), 'html.parser')

        group_link_list = soup.find_all('a', {'class': 'btn btn-sm btn-default text-nowrap'})
        for link in group_link_list:
            if link.getText().strip() == self.group:
                group_link = link['href']

        if group_link is None:
            ScheduleParser.logger.error("Raising NotFoundException")
            raise NotFoundException("Couldn't find a group link")

        return group_link

    # Parses a schedule from a link
    def parse_schedule(self, next_week=False):
        ScheduleParser.logger.info(f"Starting to parse schedule, next_week={next_week}")
        lessons = []
        try:
            group_link = self.get_group_link()
        except NotFoundException:
            ScheduleParser.logger.error("Raising CouldntParseException")
            raise CouldntParseException()

        soup = BeautifulSoup(self.load_page(self.base_url + group_link), 'html.parser')

        # Get the week type (numerator or denominator)
        week_header = soup.find('div', {'class': 'page-header'}).find('h4').find('i').getText()
        week_number = int(week_header.split(' ')[0].strip())
        week_type = week_header.split(',')[1].strip().lower()
        week_class = ''
        if (week_type == 'числитель') & (not next_week):
            week_class = 'text-success'
        elif (week_type == 'знаменатель') & (not next_week):
            week_class = 'text-info'
        elif (week_type == 'числитель') & next_week:
            week_class = 'text-info'
            week_number += 1
            week_header = str(week_number) + ' неделя, знаменатель'
        elif (week_type == 'знаменатель') & next_week:
            week_class = 'text-success'
            week_number += 1
            week_header = str(week_number) + ' неделя, числитель'
        else:
            ScheduleParser.logger.error("Raising UnknownWeekTypeException")
            raise UnknownWeekTypeException("Couldn't parse type of week in page header")

        days = soup.find_all('tbody')
        for day in days:
            day_of_week = day.find_all('td', {'class': 'bg-grey'})[0].find('strong').getText()
            rows = day.find_all('tr')
            for row in rows:
                try:
                    time = row.find('td', {'class': 'bg-grey text-nowrap'}).getText()
                    special_name = row.find('td', {'class': week_class})
                    if special_name is not None:
                        lessons.append(Lesson(special_name.getText(), day_of_week, time, week_type))
                    else:
                        name = row.find('td', {'colspan': '2'}).getText()
                        lessons.append(Lesson(name, day_of_week, time, week_type))
                except AttributeError:
                    ScheduleParser.logger.error("AttributeError occurred", exc_info=True)
                    pass

        ScheduleParser.logger.info(f"Finished parsing schedule, next_week={next_week}")
        return (week_header, lessons)

    # Parses schedule for a specific weekday
    def parse_schedule_for_one_day(self, weekday):
        ScheduleParser.logger.info(f"Starting to parse schedule for one day")
        lessons = []
        try:
            group_link = self.get_group_link()
        except NotFoundException:
            ScheduleParser.logger.error("Raising CouldntParseException")
            raise CouldntParseException()

        soup = BeautifulSoup(self.load_page(self.base_url + group_link), 'html.parser')

        # Get the week type (numerator or denominator)
        week_type = soup.find('div', {'class': 'page-header'}).find('h4').find('i').getText().split(',')[1][1:3] \
            .strip().lower()
        week_class = ''
        if week_type == 'чи':
            week_class = 'text-success'
        elif week_type == 'зн':
            week_class = 'text-info'
        else:
            ScheduleParser.logger.error("Raising UnknownWeekTypeException")
            raise UnknownWeekTypeException("Couldn't parse type of week in page header")

        days = soup.find_all('tbody')
        for day in days:
            day_of_week = day.find_all('td', {'class': 'bg-grey'})[0].find('strong').getText()
            if day_of_week != weekday:
                continue

            rows = day.find_all('tr')
            for row in rows:
                try:
                    time = row.find('td', {'class': 'bg-grey text-nowrap'}).getText()
                    special_name = row.find('td', {'class': week_class})
                    if special_name is not None:
                        lessons.append(Lesson(special_name.getText(), day_of_week, time, week_type))
                    else:
                        name = row.find('td', {'colspan': '2'}).getText()
                        lessons.append(Lesson(name, day_of_week, time, week_type))
                except AttributeError:
                    ScheduleParser.logger.error("AttributeError occurred", exc_info=True)
                    pass

        ScheduleParser.logger.info(f"Finished parsing schedule for one day")
        return lessons
