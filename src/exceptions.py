class NotFoundException(Exception):
    pass


class UnknownWeekTypeException(Exception):
    pass


class CouldntParseException(Exception):
    pass


class IncorrectWeekDayException(Exception):
    pass


class GroupNotFoundException(Exception):
    pass
